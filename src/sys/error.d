/******************************************************************************
* Project: DAW Audio
* Module:  Error
* Purpose: System error handler
* Author:  Luca D'Anzelmo <ldanzelmo@gmail.com>
* License: Boost 1.0
* Version: 0.0.1
******************************************************************************/
module sys.error;

import std.stdio;
import std.exception;
import std.path;
import std.file;
import std.experimental.logger;
import core.stdc.errno;

import fs.path;

//linux system error template
void syserror(T:int, T2:string)(T errno, T2 msg)
{
    fs.Path p = new Path;
    Logger fLogger = new FileLogger(p.osPath("./logs/debug.log"));
    switch (errno)
    {
        case EPERM:
        case EACCES:
            fLogger.log("Permission denied: ", "\t", errno, "\t", msg);
            debug writeln("Permission denied: ", "\t", errno, "\t", msg);
        break;
        case ENOENT:          
            fLogger.log("Application Error: ", "\t", errno, "\t", msg);
            debug writeln("Application Error: ", "\t", errno, "\t", msg);
        break;
        default:
            // Handle other errors
        break;
     }  
}

