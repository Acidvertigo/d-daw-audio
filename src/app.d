/******************************************************************************
* Project: DAW Audio
* Module:  app
* Purpose: Main application Entrypoint.
* Author:  Luca D'Anzelmo <ldanzelmo@gmail.com>
* License: Boost 1.0
* Version: 0.0.1
******************************************************************************/
module app;

import std.stdio;
import std.path;
import std.string;
import std.file;
import std.exception;
import std.experimental.logger;

//import dlangui;

import fs.file;
import fs.path : Path;

immutable(char[6]) str = "World!";

void main()
{
   fs.Path p = new Path;
   try
   {
      // change working directory one level up
      chdir(chomp(dirName(thisExePath()) , "/bin"));
      debug writeln("Current working directory is: ", getcwd());

      writeln("Hello : ", str);
      fs.File fl = new fs.File(p.osPath("./src/app.d"));
      fl.info();
   }
   catch (Exception ex)
   {
      Logger fLogger = new FileLogger(p.osPath("./logs/debug.log"));
      fLogger.log("Application Error: ", ex.msg);
      debug writeln("Application Error: ", ex.msg);
   }
}
