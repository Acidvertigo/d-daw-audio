/******************************************************************************
* Project: DAW Audio
* Module:  Path
* Purpose: directory operations class.
* Author:  Luca D'Anzelmo <ldanzelmo@gmail.com>
* License: Boost 1.0
* Version: 0.0.1
******************************************************************************/
module fs.path;

import std.stdio;
import std.path;
import std.file;
import std.array;
import std.system;
import std.string;

import sys.error;

interface Dirs
{
   void explore(string path);
   void newDir(string path);
   void delDir(string path);
   string osPath(string path);
}

class Path : Dirs
{
   //recursive directory explorator
   @system void explore(string path)
   {
      foreach (DirEntry e; dirEntries(this.osPath(path), SpanMode.breadth))
      {
         writeln(e.name, "\t", e.size, "\t", e.timeLastAccessed);
      }
   }

   ///
   unittest
   {
      Path path = new Path;
      path.explore("./src");
   }

   //create a new dir
   @system void newDir(string path)
   {
      try
      {
         mkdir(this.osPath(path));
      }
      catch (FileException ex)
      {
         sys.error.syserror(ex.errno, ex.msg);
      }
   }

   ///
   unittest
   {
      Path path = new fs.Path;
      path.newDir("./testdir");
      assert(isDir(path.osPath("./testdir")));
      assert(exists(path.osPath("./testdir")));
   }

   // deletes a directory
   @system void delDir(string path)
   {
      try
      {
         rmdir(this.osPath(path));
      }
      catch (FileException ex)
      {
         sys.error.syserror(ex.errno, ex.msg);
      }
   }

   ///
   unittest
   {
      Path path = new fs.Path;
      path.delDir("./testdir");
      assert(!exists(path.osPath("./testdir")));
   }
   
   // Converts between Windows and Posix path.
   // Returns unaltered string if os is the same
   // of path type.
   @system string osPath(string path)
   {
       version(Posix)
       {
           if ( indexOf(path, ":") > 0 )
           {
               try
               {
                   path = "." ~ path[indexOf(path, `\`)  .. $];
                   path = path.replace(r"\", "/");
               }
               catch (FileException ex)
               {
                   sys.error.syserror(ex.errno, ex.msg);
               }
           }
       }
       version(Windows)
       {
           if ( std.path.driveName(path).empty )
           {
               try
               { 
                   path = path[indexOf(path, ".") .. $];
                   path = path.replace("/", r"\");
                   path = dirName(path);                   
               }
               catch (FileException ex)
               {
                   sys.error.syserror(ex.errno, ex.msg);
               }
           }
       }
       return path;
   }

   ///
   unittest
   {
      Path path = new Path;
      version(Posix)
      {
          assert(path.osPath("c:\\src") == "./src");
          assert(path.osPath("c:\\src\\foo\\bar") == "./src/foo/bar");
          assert(path.osPath("./src") == "./src");
          assert(path.osPath("./src/foo/bar") == "./src/foo/bar");
      }
      version (Windows)
      {
          writeln(path.osPath("./src"));
          assert(path.osPath("c:\\src") == "c:\\src");
          assert(path.osPath("c:\\src\\foo\\bar") == "c:\\src\\foo\\bar");
          assert(path.osPath("./src") == getcwd() ~ "\\src");
          assert(path.osPath("./src/foo/bar") == getcwd() ~ "\\src\\foo\\bar");
      }
   }
}

///
unittest
{
   Path path = new Path;
   path.explore("./src/test");
}

