/******************************************************************************
* Project: DAW Audio
* Module:  File
* Purpose: file operation class.
* Author:  Luca D'Anzelmo <ldanzelmo@gmail.com>
* License: Boost 1.0
* Version: 0.0.1
******************************************************************************/
module fs.file;

import std.stdio;
import std.file;
import std.path;
import std.datetime : SysTime;
import std.exception;
import core.stdc.errno;

import fs.path : Path;
import sys.error;

interface Files
{
   string name();
   bool isDir();
   bool isFile();
   bool isSimlink();
   ulong size();
   SysTime timeLastAccessed();
   SysTime timeLastModified();
   uint attributes();
   void cp(string dest);
   void del(string file);
   void newEmpty(string filename);
   void info();
}

class File : Files
{
   private std.file.DirEntry _file;  //file resource struct
   public  fs.Path p;

   // print on screen info about the file
   @system void info()
   {
      writefln("File name:          %s", this._file.name);
      writefln("Is directory:       %s", this._file.isDir);
      writefln("Is file:            %s", this._file.isFile);
      writefln("Is symlink:         %s", this._file.isSymlink);
      writefln("Size in bytes:      %s", this._file.size);
      writefln("Time last accessed: %s", this._file.timeLastAccessed);
      writefln("Time last modified: %s", this._file.timeLastModified);
      writefln("Attributes:         %b", this._file.attributes);
   }

   // generic properties getters for File struct
   @property string name()              { return this._file.name; }
   @property bool isDir()               { return this._file.isDir; }
   @property bool isFile()              { return this._file.isDir; }
   @property bool isSimlink()           { return this._file.isFile; }
   @property ulong size()               { return this._file.size; }
   @property SysTime timeLastAccessed() { return this._file.timeLastAccessed; }
   @property SysTime timeLastModified() { return this._file.timeLastModified; }
   @property uint attributes()          { return this._file.attributes; }

   //create a new empty file
   @system void newEmpty(string filename)
   {
      try
      {
         std.stdio.File(this.p.osPath(filename), "w");
      }
      catch (ErrnoException ex)
      {
         sys.error.syserror(ex.errno, ex.msg);
      }
   }

   ///
   unittest
   {
      fs.File fl = new fs.File;
      Path path = new Path;
      fl.newEmpty("./src/test/dummyempty.tst");
      assert(exists(path.osPath("./src/test/dummyempty.tst")));
   }

   //copy file to destination
   @system void cp(string dest)
   {
      try
      {
         copy(_file.name, this.p.osPath(dest));
      }
      catch (FileException ex)
      {
         sys.error.syserror(ex.errno, ex.msg);
      }
   }

   ///
   unittest
   {
      fs.File fl = new fs.File("./src/test/dummy.tst");
      Path path = new Path;
      fl.cp("./src/test/dummycopy.tst");
      assert(exists(path.osPath("./src/test/dummycopy.tst")));
   }

   // rename a file
   @system void rename(string filename)
   {
      try
      {   
         std.file.rename(this._file.name, this.p.osPath(filename));
      }
      catch (FileException ex)
      {
         sys.error.syserror(ex.errno, ex.msg);
      }
   }

   ///
   unittest
   {
      fs.File fl = new fs.File("./src/test/dummycopy.tst");
      Path path = new Path;
      fl.rename("./src/test/dummyrenamed.tst");
      assert(exists(path.osPath("./src/test/dummyrenamed.tst")));
   }
   
   @system string getExtension(string filename)
   {
       return extension(filename);
   }
   
   unittest
   {
       fs.File fl = new fs.File;
       assert (fl.getExtension("file.tst") == ".tst");
   }

   //delete a file
   @system void del(string filename)
   {
      try
      {
         filename = this.p.osPath(filename);
         remove(filename);
      }
      catch (FileException ex)
      {
         sys.error.syserror(ex.errno, ex.msg);
      }
   }

   ///
   unittest
   {
      fs.File fl = new fs.File;
      Path path = new Path;
      fl.del("./src/test/dummyrenamed.tst");
      fl.del("./src/test/dummyempty.tst");
      assert(!exists(path.osPath("./src/test/dummyrenamed.tst")));
      assert(!exists(path.osPath("./src/test/dummyempty.tst")));
   }

   // class object constructor
   this(string path = null)
   {
      try
      {
         this.p = new fs.Path;
         this._file = DirEntry(this.p.osPath(path));
      }
      catch (FileException ex)
      {
         sys.error.syserror(ex.errno, ex.msg);
      }
   }
}

///
unittest
{
   fs.File instance = new fs.File("./src/test/dummy.tst");
   instance.info();
}
