/******************************************************************************
* Project: DAW Audio
* Module:  File package
* Purpose: Package fs definition.
* Author:  Luca D'Anzelmo <ldanzelmo@gmail.com>
* License: Boost 1.0
* Version: 0.0.1
******************************************************************************/
module fs;

public:
import fs.file;
import fs.path;
