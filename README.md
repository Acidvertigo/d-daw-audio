# D-DAW Audio # [ ![Codeship Status for Acidvertigo/d-daw-audio](https://app.codeship.com/projects/d592dc60-7147-0134-64ba-7ead778a38d2/status?branch=master)](https://app.codeship.com/projects/178332)

An audio DAW developed in D language.

### What is this repository for? ###

* Just an attemp to build a working audio DAW environment in D language.
* Version: 0.0.1 Pre-Pre-Alpha
* License: [Boost 1.0](https://bitbucket.org/Acidvertigo/d-daw-audio/src/060e2284c6c761b84e3c563eebb0ccc07d088d0e/license?at=master&fileviewer=file-view-default)
* [Bitbucket Repository](https://bitbucket.org/Acidvertigo/d-daw-audio)

### How do I get set up? ###

* Download [dmd](https://dlang.org/download.html) D language compiler 
  or run the following installation script ```curl -fsS https://dlang.org/install.sh | bash -s dmd```
* TODO: Configuration
* Dependencies: dlangui.
* To run tests: ```dub -b unittest -c [os]``` where [os] can be linux or win.
* TODO: Deployment instructions

### Contribution guidelines ###

* TODO: Writing tests
* TODO: Code review
* TODO: Other guidelines
