###############################################################################
# Project: DAW Audio
# Purpose: Main Project makefile
# Author:  Luca D'Anzelmo
# License: MIT License
# Version: 0.0.1
###############################################################################
compiler=ldc2
linker=ldc2

target=../bin/Debug/dtest

objects = ../obj/Debug/file.o ../obj/Debug/path.o ../obj/Debug/package.o ../obj/Debug/main.o

DLFAGS=-d-debug -oq -mcpu=core2

all: $(target)

$(target): $(objects)
	@echo Linking...
	$(linker) -d-debug "-of$@" $(objects)

../obj/Debug/file.o : fsis/file.d
	$(compiler) $(DFLAGS) -c $? "-of$@" "-I../DerelictUtil" -gc 

../obj/Debug/path.o : fsis/path.d
	$(compiler) $(DFLAGS) -c $? "-of$@" "-I../DerelictUtil" -gc 

../obj/Debug/package.o : fsis/package.d
	$(compiler) $(DFLAGS) -c $? "-of$@" "-I../DerelictUtil" -gc 

../obj/Debug/main.o : main.d
	$(compiler) $(DFLAGS) -c $? "-of$@" "-I../DerelictUtil" -gc 

clean:
	$(RM) "$(target)" $(objects)
